# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np

from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import StandardScaler

from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV

from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier


### 1.PRE PROCESSING

# Fonction de pre preocessing qui utilise toutes les sous fonctions codées par la suite 
def preprocessing(chemin_acces):
    return mise_a_echelle_standard(transformer_chaines_en_nombres(filtre_classe(remplir_valeurs_manquantes(verifie_etiquettes_colonnes(chemin_acces)))))

# Fonction pour vérifier si des étiquettes de colonnes existent
def verifie_etiquettes_colonnes(chemin_acces_fichier):
    # Charger les données depuis le fichier CSV
    donnees = pd.read_csv(chemin_acces_fichier)
    
    try: 
        # Parcourir toutes les colonnes des données
        for colonne in donnees.columns:
            # Tenter de convertir la valeur de la colonne en flottant
            if float(colonne):
                # Si la conversion réussit, cela indique l'absence d'étiquettes de colonnes
                # Retourner les données sans en-tête (header=None)
                return pd.read_csv(chemin_acces_fichier, header=None)
    except:
        # Si une exception se produit (par exemple, la conversion en flottant échoue),
        # cela indique la présence d'étiquettes de colonnes
        # Retourner les données telles quelles
        return donnees


# Fonction pour remplir les valeurs manquantes
def remplir_valeurs_manquantes(donnees):
    # Parcourir toutes les colonnes des données
    for colonne in donnees.columns:
        # Vérifier le type de données de la colonne
        if donnees[colonne].dtypes == object:
            # Si le type est 'object' (texte), remplir les valeurs manquantes avec la valeur la plus fréquente
            valeur_plus_frequente = donnees[colonne].value_counts().sort_values(ascending=False).index[0]
            donnees[colonne].fillna(valeur_plus_frequente, inplace=True)  
        elif donnees[colonne].dtypes == float or donnees[colonne].dtypes == int:
            # Si le type est 'float' ou 'int', remplir les valeurs manquantes avec la médiane (ou la moyenne)
            # On peut remplacer la médiane par la moyenne
            donnees[colonne].fillna(donnees[colonne].median(), inplace=True)
    
    # Retourner les données après avoir rempli les valeurs manquantes
    return donnees

#Fonction filtrant les classes sous-représentées
def filtre_classe(donnees):
    etiquette_col = donnees.columns[-1]
    
    #Calcul des proportions représentées par chaque classe
    proportions = donnees[etiquette_col].value_counts(normalize=True)
    index_classes = proportions.index
    proportions_classes = proportions.values
    
    filtre = []
    #Selection des classes à supprimer
    for j in range(len(proportions)):
        if proportions_classes[j]<0.1:
            filtre.append(index_classes[j])
    
    #Filtrage
    if len(filtre)>0:
        donnees.drop(donnees[donnees[etiquette_col].isin(filtre)].index, inplace = True)
    
    return donnees

# Fonction pour transformer les chaînes de caractères en nombres
def transformer_chaines_en_nombres(donnees):
    # Identifier les colonnes contenant des chaînes de caractères
    colonnes_str = donnees.columns[donnees.dtypes == object]
    
    # Parcourir les colonnes de chaînes de caractères
    for colonne in colonnes_str:
        # Utiliser LabelEncoder pour attribuer des valeurs numériques aux chaînes de caractères
        donnees[colonne] = LabelEncoder().fit_transform(donnees[colonne])
    
    # Retourner les données après la transformation des chaînes en nombres
    return donnees


# Fonction pour mettre à l'échelle les données
def mise_a_echelle_standard(donnees):
    # Exclure la colonne cible que nous ne voulons pas mettre à l'échelle
    donnees_a_echelle = donnees.drop(columns=donnees.columns[-1])
    
    # Appliquer StandardScaler pour mettre à l'échelle les valeurs
    donnees_a_echelle = pd.DataFrame(StandardScaler().fit_transform(donnees_a_echelle.values),
                                     columns=donnees_a_echelle.columns, index=donnees_a_echelle.index)
    
    # Ajouter la colonne cible à nouveau aux données mises à l'échelle
    donnees_a_echelle['Classe'] = donnees.iloc[:, -1]
    
    # Retourner les données après la mise à l'échelle
    return donnees_a_echelle

### 2.Fonction pour préparer le data set pour l'entrainement
def diviser_jeu_de_donnees(data, taille_test):
    # Séparation des caractéristiques (X) et des étiquettes (y)
    X = data.drop(columns=data.columns[-1])
    y = data.iloc[:, -1]

    # Division des données en ensembles d'entraînement et de test
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=taille_test)

    return X_train, X_test, y_train, y_test

### Modèles
def DecisionTree_gridsearch(X_train, X_test, y_train, y_test):
    """
    Applique une recherche sur grille pour ajuster les hyperparamètres d'un classificateur d'arbre de décision.

    Paramètres :
    - X_train : DataFrame, ensemble d'entraînement des caractéristiques.
    - X_test : DataFrame, ensemble de test des caractéristiques.
    - y_train : Series, ensemble d'entraînement des étiquettes.
    - y_test : Series, ensemble de test des étiquettes.

    Retourne :
    - y_pred : ndarray, les prédictions du modèle sur l'ensemble de test.
    - param_best : dict, les meilleurs hyperparamètres trouvés par la recherche sur grille.
    """
    # Définition des hyperparamètres à explorer
    parametres_arbre = {'criterion': ['gini', 'entropy'], 'max_depth': np.arange(2, 15)}

    # Création du classificateur avec recherche sur grille
    classificateur = GridSearchCV(
        estimator=DecisionTreeClassifier(),
        param_grid=parametres_arbre,
        scoring='accuracy',
        n_jobs=-1,
        cv=5
    )

    # Entraînement du modèle
    classificateur.fit(X_train, y_train)

    # Prédictions sur l'ensemble de test
    y_pred = classificateur.predict(X_test)

    # Meilleurs hyperparamètres trouvés
    tuned_param = classificateur.best_params_

    return y_pred, tuned_param

def RandomForest_gridsearch(X_train, X_test, y_train, y_test):
    # Définition de la grille des hyperparamètres à ajuster
    parameters_randomforest = {
        'bootstrap': [True, False],                  # Utiliser ou non le bootstrap
        'max_depth': np.arange(2, 10, 2),           # Profondeur maximale de l'arbre
        'max_features': [2, 3],                     # Nombre maximal de caractéristiques à considérer pour le fractionnement
        'min_samples_leaf': [1, 2, 3, 4, 5],        # Nombre minimal d'échantillons dans une feuille
        'min_samples_split': [2],                   # Nombre minimal d'échantillons requis pour fractionner un nœud
        'n_estimators': [50, 100, 200]             # Nombre d'arbres dans la forêt
    }  

    # Initialisation du classificateur Random Forest
    classifier = GridSearchCV(
        estimator=RandomForestClassifier(),        # Utilisation du classificateur Random Forest
        param_grid=parameters_randomforest,         # Grille des hyperparamètres
        scoring='accuracy',                         # Métrique de score (exactitude dans ce cas)
        n_jobs=-1,                                  # Utilisation de tous les processeurs disponibles
        cv=5                                        # Validation croisée à 5 plis
    )

    # Entraînement du modèle en utilisant la recherche fine des hyperparamètres
    classifier.fit(X_train, y_train)
    
    # Prédiction sur l'ensemble de test
    y_pred = classifier.predict(X_test)

    # Récupération des meilleurs paramètres trouvés par la recherche fine
    tuned_param = classifier.best_params_

    # Retourne les prédictions et les meilleurs paramètres
    return y_pred, tuned_param

def MLP_gridsearch(X_train, X_test, y_train, y_test):
    """
    Applique une recherche sur grille pour ajuster les hyperparamètres d'un classificateur MLP (Perceptron multi-couches).

    Paramètres :
    - X_train : DataFrame, ensemble d'entraînement des caractéristiques.
    - X_test : DataFrame, ensemble de test des caractéristiques.
    - y_train : Series, ensemble d'entraînement des étiquettes.
    - y_test : Series, ensemble de test des étiquettes.

    Retourne :
    - y_pred : ndarray, les prédictions du modèle sur l'ensemble de test.
    - param_best : dict, les meilleurs hyperparamètres trouvés par la recherche sur grille.
    """
    # Définition des hyperparamètres à explorer
    parametres_mlp = {
        'solver': ['lbfgs'],
        'max_iter': [1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000],
        'alpha': 10.0 ** -np.arange(1, 10),
        'hidden_layer_sizes': np.arange(10, 15),
        'random_state': [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    }

    # Création du classificateur avec recherche sur grille
    classificateur = GridSearchCV(
        estimator=MLPClassifier(),
        param_grid=parametres_mlp,
        scoring='accuracy',
        n_jobs=-1
    )

    # Entraînement du modèle
    classificateur.fit(X_train, y_train)

    # Prédictions sur l'ensemble de test
    y_pred = classificateur.predict(X_test)

    # Meilleurs hyperparamètres trouvés
    param_best = classificateur.best_params_

    return y_pred, param_best


### 3. Entrainement des modèles
def benchmark(df,models):
    bm = pd.DataFrame(columns=["model","parameters","X_test","y_test","y_pred"])
    k=1
    
    x_train,x_test,y_train,y_test = f.diviser_jeu_de_donnees(df,1/3)
    
    #Pour chaque model on stock les meilleurs hyperparamètres ainsi que les données de test et d'entrainement associées
    #qui serviront pour l'évaluation
    for m in models: 
        y_pred,tuned_param = m(x_train,x_test,y_train,y_test)
        bm.loc[k] = [m.__name__,tuned_param,x_test,y_test,y_pred]
        k+=1
    return bm
